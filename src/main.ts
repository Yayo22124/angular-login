import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';  // Importa tu módulo principal


platformBrowserDynamic().bootstrapModule(AppModule)  // Inicializa tu módulo principal (AppModule)
  .catch(err => console.error(err));
